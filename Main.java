package testoop;

import java.util.ArrayList;
import java.util.Scanner;
public class Main {
	static ArrayList<Computer> computer = new ArrayList<>();
	static Scanner insert = new Scanner(System.in);

	public static void main(String[] args) {
	
		Computer computer1 = new Computer("HP", 1);
		Computer computer2 = new Computer("ASUS", 2);
		Computer computer3 = new Computer("MSI", 3);
		Computer computer4 = new Computer("ACER", 4);
		Computer computer5 = new Computer("DELL", 4);
		
		
		computer.add(computer1);
		computer.add(computer2);
		computer.add(computer3);
		computer.add(computer4);
		computer.add(computer5);
		
		
		Delete();

	}

	public static void delete(int Numcomputer) {
		computer.remove(Numcomputer - 1);
		System.out.print(" Computer = "+ "Delete");
		for (Computer ComputerList : computer) {
			System.out.print("\n" + ComputerList);
		}
	}

	public static void Delete() {
		int yourNumcom;
		System.out.print("Please insert Number Computer Delete  :");		
		boolean Program = false;
		while (Program == false)
			try {
				yourNumcom = Integer.parseInt(insert.nextLine());
				delete(yourNumcom);
				break;
			} catch (NumberFormatException nfe) {
				System.out.print("NumCom Try again: ");
			}
		Program = true;
	}
}
